<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->app->version();
});


$app->put('api/splashes/{splash}', 'Api\SplashController@update');
$app->post('api/splashes/create', 'Api\SplashController@store');
$app->get('api/splashes', 'Api\SplashController@index');
$app->get('api/splashes/{splash}', 'Api\SplashController@show');
$app->delete('api/splashes/{splash}', 'Api\SplashController@destroy');
