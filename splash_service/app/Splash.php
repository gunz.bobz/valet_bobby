<?php



namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;


class Splash extends Model 
{
 use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array   */
    protected $guarded = [];
    protected $hidden = ['deleted_at','deleted_by'];
    protected $table = 'splash';
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
 
}
