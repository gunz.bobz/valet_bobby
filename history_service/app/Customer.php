<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    protected $guarded=[];
    protected $connection='customer';
    protected $table='customers';
    protected $hidden=['created_at','deleted_at','updated_at'];
    public function Inquery()
    {
    return $this->belongsTo('App\Inquery', 'id');
    }
    
}
