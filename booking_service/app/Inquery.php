<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Inquery extends Model 
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard = [];
    // protected $visible = ['id','full_name','telp','email'];
    protected $table = 'inqueries';
    protected $connection= 'mysql3';
}
