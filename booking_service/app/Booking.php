<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model 
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    protected $hidden=['deleted_at'];
    protected $table='bookings';
    // protected $connection ='mysql';
    public function getState()
    {
        return $this->hasOne('App\States', 'id', 'state');
    }    
    public function GetName()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }   





}
