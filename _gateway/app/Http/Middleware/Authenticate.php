<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Tymon\JWTAuth\JWTAuth;



class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;
    protected $jwt;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
        // $this->jwt = $jwt;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        // $token = $request->get('token');
        
        // if(!$token) {
        //     // Unauthorized response if token not there
        //     return response()->json([
        //         'STATUS' => '401',
        //         'token' => $token,
        //         'MESSAGE' => 'Token not provided.'
        //     ], 401);
        // }



        if ($this->auth->guard($guard)->guest()) {
            $res= ['STATUS' => '401',
            'MESSAGE' => 'Unauthorized',
            
            // 'expires' => Factory::getTTL() * 60
        ];


             return $res;
        
             
             
             
            }
        
        return $next($request);
}
}