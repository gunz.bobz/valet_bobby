<?php

namespace App\Http\Controllers;
use Illuminate\Auth\Passwords\PasswordBrokerManager;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Carbon\Carbon;
// use Tymon\JWTAuth\JWTAuth;

use App\User;
use App\Traits\ResetsPasswords;
class AuthController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    use ResetsPasswords;
    protected $jwt;


    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
        $this->broker = 'users';
    }

        // 1. Send reset password email
    public function generateResetToken(Request $request)
    {
        // Check email address is valid
        $this->validate($request, ['email' => 'required|email|exists:users']);

        // Send password reset to the user with this email address
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
            ? response()->json(true)
            : response()->json(false);
    }

 // 2. Reset Password
 public function resetPassword(Request $request)
 {
     // Check input is valid
     $rules = [
         'token'    => 'required',
         'username' => 'required|string',
         'password' => 'required|confirmed|min:6',
     ];
     $this->validate($request, $rules);

     // Reset the password
     $response = $this->broker()->reset(
     $this->credentials($request),
         function ($user, $password) {
             $user->password = app('hash')->make($password);
             $user->save();
         }
     );

     return $response == Password::PASSWORD_RESET
         ? response()->json(true)
         : response()->json(false);
 }

 /**
  * Get the password reset credentials from the request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return array
  */
 protected function credentials(Request $request)
 {
     return $request->only('username', 'password', 'password_confirmation', 'token');
 }

 /**
  * Get the broker to be used during password reset.
  *
  * @return \Illuminate\Contracts\Auth\PasswordBroker
  */
 public function broker()
 {
     $passwordBrokerManager = new PasswordBrokerManager(app());
     return $passwordBrokerManager->broker();
 }
    


    public function register(Request $request)
    {
        //validate incoming request 
        $this->validate($request, [
            'name' => 'required|string',
            'telp' => 'required|string|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);
        try {
           
            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);
            $user->telp =  $request->input('telp');
            $user->save();
            //return successful response
            return response()->json(['user' => $user, 'message' => 'CREATED'], 201);
        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => 'User Registration Failed!'], 409);
        }
    }
  


    
    public function postLogin(Request $request)
    {
        
        $this->validate($request, [
            'email'    => 'required|email|max:255',
            'password' => 'required',
        ]);

        try {

            // if (! $token = $this->jwt->attempt($request->only('email', 'password'))) {
            if (! $token = $this->jwt->attempt($request->only('email', 'password'))) {
                $res= ['STATUS' => '404',
                       'MESSAGE' => 'Invalid email or password',
                
                    ];
                return $res;
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent' => $e->getMessage()], 500);

        }

        $data = User::where('email',$request->email)->get();
        $exp = $this->jwt->setToken($token)->getPayload()->get('exp');
        // $payload = $this->jwt->decode($token);
        $expirationTime = date('d M Y H:i:s ', $exp);
        $date = date('m/d/Y h:i:s a', time());

        $now = Carbon::now(+8);
        $timezone = $now->timezone;

        $data->makeHidden(['updated_at','deleted_at','verified_at','remember_token']);
        // $payload = JWTAuth::parseToken()->getPayload();
        // $expires_at = date('d M Y h:i', $payload->get('exp')); 
        $res= ['STATUS' => '200',
        'MESSAGE' => 'user logged in',
        'DATA' => $data,
        'token' => $token,
        'expires' => $expirationTime,
        'date' => $now,
        'tz' => $timezone
        // 'expires' => Factory::getTTL() * 60
    ];
         return $res;
        // return response()->json(compact('token'));
    }
}