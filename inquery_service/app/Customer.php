<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Customer extends Model
{
    // use SoftDeletes;
    protected $table='customers';
    protected $guarded=[];
    protected $hidden=['id','created_at','updated_at'];


}
