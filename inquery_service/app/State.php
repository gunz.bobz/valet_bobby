<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    // use SoftDeletes;
    protected $table='states';
    protected $guarded=[];
    protected $hidden=['id','created_at','deleted_at','updated_at'];


}
